import logo from "./logo.svg";
import "./App.css";

import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Divider from "@mui/material/Divider";

import Display1 from "./components/Display1";
import Display2 from "./components/Display2";
import Display3 from "./components/Display3";

function App() {
  return (
    <div className="container">
      <Box sx={{ height: 300, width: 1200, border: 2 }}>
        <h3
          style={{ textAlign: "center", margin: "20px", marginBottom: "70px" }}
        >
          Conditional Rendering
        </h3>
        <Grid container>
          <Grid item>
            <Display1 />
          </Grid>

          <Grid item>
            <Display2 />
          </Grid>
          <Grid item>
            <Display3 />
          </Grid>
        </Grid>
      </Box>
    </div>
  );
}

export default App;
