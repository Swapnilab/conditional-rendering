import React, { useState } from "react";
import Box from "@mui/material/Box";
import Divider from "@mui/material/Divider";
import Switch from "@mui/material/Switch";

const Display1 = () => {
  const [isOn, setIsOn] = useState(false);
  return (
    <div>
      <Box sx={{ height: 300, width: 380, textAlign: "center" }}>
        {isOn ? <h1>ON</h1> : <h1>OFF</h1>}
        <Switch
          defaultValue={isOn}
          onChange={() => {
            isOn ? setIsOn(false) : setIsOn(true);
          }}
        />

        <Divider orientation="vertical" />
      </Box>
    </div>
  );
};

export default Display1;
